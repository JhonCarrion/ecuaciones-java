/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolver_ecuaciones;

import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Usuario
 */
public class Resolver_Ecuaciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Scanner teclado = new Scanner(System.in);
        //System.out.print("Ingrese el grado: ");
        int grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado del polinomio"));
        //System.out.print("minimo: ");
        int minimo = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor mínimo del intervalo"));
        //System.out.print("maximo: ");
        int maximo = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor máximo del intervalo"));
        //System.out.print("Incremento: ");
        double inc = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el valor del incremento"));
        double[] raices = new double[grado];
        long coeficientes[] = new long[grado + 1];
        for (int i = 0; i <= grado; i++) {
            //System.out.print("Ingrese el valor para a" + i + "x^" + (grado - i) + ": ");
            coeficientes[i] = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor para a" + i + "x^" + (grado - i) + ": "));
        }
        //System.out.println("");
        //System.out.println("Ecucion: ");
        String ecuacion = coeficientes[0] + "x^" + (grado);
        System.out.print(ecuacion);
        for (int i = 1; i <= grado; i++) {
            if ((grado - i) != 0) {
                ecuacion += " + " + coeficientes[i] + "x^" + (grado - i);
                System.out.print(" + " + coeficientes[i] + "x^" + (grado - i));
            } else {
                if (coeficientes[i] != 0) {
                    ecuacion += " + " + coeficientes[i];
                }
                System.out.print(" + " + coeficientes[i]);
            }
        }
        System.out.println("");
        System.out.println("Tabla de valores");

        DefaultCategoryDataset datos = new DefaultCategoryDataset();
        System.out.println("x | y");
        String tabla_valores = "x  |  y\n";
        int j = 0;
        String raicesStr = "";
        for (double i = minimo; i <= maximo; i += inc) {
            double suma = 0;
            for (int k = 0; k <= grado; k++) {
                suma += coeficientes[k] * Math.pow(i, (grado - k));
            }
            String nbsp = "";
            if (i >= 0) {
                nbsp = " ";
            }
            tabla_valores += nbsp + i + " | " + nbsp + suma + "\n";
            System.out.println(i + " | " + suma);
            datos.addValue(suma, "x", String.valueOf(i));
            if (suma == 0) {
                raices[j] = i;
                raicesStr += "X" + (j + 1) + " = " + i + "; Y" + (j + 1) + " = " + suma + "\n";
                j++;
            }
        }
        JOptionPane.showMessageDialog(null, tabla_valores, "TABLA DE VALORES", JOptionPane.INFORMATION_MESSAGE);
        JFreeChart grafica = ChartFactory.createLineChart(ecuacion, "x", "y", datos, PlotOrientation.VERTICAL, true, true, false);
        ChartPanel Panel = new ChartPanel(grafica);
        JFrame Ventana = new JFrame("Plot");
        Ventana.getContentPane().add(Panel);
        Ventana.pack();
        Ventana.setVisible(true);
        Ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (j != raices.length) {
            JOptionPane.showMessageDialog(null, "No se extrajeron todas las raices de la función.\nLa ecuación podria tener raices Imaginarias", "FALTAN RAICES", JOptionPane.WARNING_MESSAGE);
            int opt = Integer.parseInt(JOptionPane.showInputDialog("¿Desea tratar de obtener las raices?\nIngrese 1 si es asi, de lo contrario ingrese cualquier número"));
            if (opt == 1) {
                DefaultCategoryDataset datos2 = new DefaultCategoryDataset();
                double i = minimo;
                int cont = 0;
                j = 0;
                raicesStr = "";
                System.out.println("x | y");
                while (j != raices.length && cont < 500) {
                    if (i > maximo) {
                        i = minimo;
                        inc /= 2;
                        cont++;
                        j = 0;
                        raicesStr = "";
                        System.out.println("iteración: " + cont);
                    }
                    double suma = 0;
                    for (int k = 0; k <= grado; k++) {
                        double cuadrado = Math.pow(i, (grado - k));
                        suma += coeficientes[k] * cuadrado;
                    }
                    //System.out.println(i + " | " + suma);
                    datos2.addValue(suma, "x", String.valueOf(i));
                    if (suma == 0.0) {
                        raices[j] = i;
                        raicesStr += "X" + (j + 1) + " = " + i + "\n";
                        j++;
                    }
                    i += inc;
                }
                Ventana.setVisible(false);
                JFreeChart grafica2 = ChartFactory.createLineChart(ecuacion, "x", "y", datos2, PlotOrientation.VERTICAL, true, true, false);
                ChartPanel Panel2 = new ChartPanel(grafica2);
                JFrame Ventana2 = new JFrame("Plot");
                Ventana2.getContentPane().add(Panel);
                Ventana2.pack();
                Ventana2.setVisible(true);
                Ventana2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                if (j != raices.length) {
                    JOptionPane.showMessageDialog(null, "No se extrajeron todas las raices de la función despues de realizar " + cont + " iteraciones, dividiendo el incremento a la mitad en cada una.\ninc: " + inc + "\nLa ecuación podria tener raices Imaginarias", "FALTAN RAICES", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        JOptionPane.showMessageDialog(null, "Las raices halladas son:\n" + raicesStr, "RAICES ENCONTRADAS", JOptionPane.INFORMATION_MESSAGE);

    }

}
